import { Component } from '@angular/core';
import { LugaresService } from '../services/lugares.services';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { FormControl } from '@angular/forms';
import { Http } from '@angular/http';

@Component({
	selector: 'app-crear',
	templateUrl: './crear.component.html',
	styleUrls: ['./crear.component.css']
})
export class CrearComponent {
	lugar:any = {};
	id:any = null;
	private searchField: FormControl;
	results$: Observable<any>;
	constructor(private lugaresService: LugaresService, private route: ActivatedRoute, private http: Http){
		this.id = this.route.snapshot.params['id'];
		if(this.id != 'new'){
			this.lugaresService.getLugar(this.id)
				.valueChanges().subscribe((lugar) => {
					this.lugar = lugar;
				});
		}
		const URL = 'https://maps.google.com/maps/api/geocode/json';
		this.searchField = new FormControl();
		this.results$ = this.searchField.valueChanges
			.debounceTime(500)
			.switchMap(query => this.http.get(`${URL}?address=${query}`))
			.map(response => response.json())
			.map(response => response.results);
	}
	seleccionarDireccion(direccion){
		//console.log(direccion);
		/*this.lugar.calle = direccion.address_components[1].long_name + ' ' + direccion.address_components[0].long_name;
		this.lugar.ciudad = direccion.address_components[4].long_name;
		this.lugar.pais = direccion.address_components[6].long_name;*/
		const addressComponents = direccion.address_components
		const adrressParams: any = {}
		for (let i = 0, len = addressComponents.length; i < len; i++) {
			const type = addressComponents[i].types[0].toString()
			switch (type) {
			case'street_number':
			adrressParams.street_number = addressComponents[i].long_name
			break
			case'route':
			adrressParams.route = addressComponents[i].long_name
			break
			case'locality':
			adrressParams.locality = addressComponents[i].long_name
			break
			case'country':
			adrressParams.country = addressComponents[i].long_name
			break
			}
		}
		this.lugar.calle = `${adrressParams.route}${adrressParams.street_number}`
		this.lugar.ciudad = adrressParams.locality
		this.lugar.pais = adrressParams.country
	}
	guardarLugar(){
		var direccion = this.lugar.calle+','+this.lugar.ciudad+','+this.lugar.pais;
		this.lugaresService.obtenerGeoData(direccion)
			.subscribe((result) => {
				if(this.id != 'new'){
					this.lugar.lat = result.json().results[0].geometry.location.lat;
				this.lugar.lng = result.json().results[0].geometry.location.lng;
					this.lugaresService.editarLugar(this.lugar);
					alert('Negocio editado con éxito');
				} else {
					this.lugar.id = Date.now();
					this.lugaresService.guardarLugar(this.lugar);
					alert('Negocio guardado con éxito');
				}
				this.lugar = {};
			});
	}
}