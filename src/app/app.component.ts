import { Component } from '@angular/core';
import { AutorizacionService } from './services/autorizacion.services';
import { Router } from '@angular/router';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	loggedIn = false;
	loggedUser:any = null;
	constructor(private autorizacionService: AutorizacionService, private router: Router){
		this.autorizacionService.isLogged()
			.subscribe((result) => {
				if(result && result.uid){
					this.loggedIn = true;
					setTimeout(()=>{
				    	this.loggedUser = this.autorizacionService.getUser().currentUser.email;
				    }, 500);
				} else {
					this.loggedIn = false;
				}
			}, (error) => {
				this.loggedIn = false
			})
	}
	logout(){
		this.autorizacionService.logout();
		alert('Sesión cerrada');
		this.router.navigate(['lugares']);
	}
}
