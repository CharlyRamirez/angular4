import { Component } from '@angular/core';

@Component({
	selector: 'app-contacto',
	templateUrl: './contacto.component.html',
	styleUrls: ['./contacto.component.css']
})
export class ContactoComponent {
	lat:number= 20.5576425;
	lng:number= -100.417527;
}