import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth/auth';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
@Injectable()
export class AutorizacionService{
	constructor(private angularFireAuth: AngularFireAuth, private router: Router){
		this.isLogged();
	}
	public facebookLogin(){
		this.angularFireAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
			.then((result) => {
				alert('Usuario loggeado con Facebook!');
				console.log(result);
				this.router.navigate(['lugares']);
			})
			.catch((error) => {
				console.log(error);
				alert(error);
			})
	}
	public login = (email, password) => {
		this.angularFireAuth.auth.signInWithEmailAndPassword(email, password)
			.then((response) => {
				alert('Usuario loggeado con éxito');
				console.log(response);
				this.router.navigate(['lugares']);
			})
			.catch((error) => {
				alert('Un error ha ocurrido');
				console.log(error);
			})
	}
	public registro = (email, password) => {
		this.angularFireAuth.auth.createUserWithEmailAndPassword(email, password)
			.then((response) => {
				alert('Usuario registrado con éxito');
				console.log(response);
				this.router.navigate(['lugares']);
			})
			.catch((error) => {
				alert('Un error ha ocurrido');
				console.log(error);
			})
	}
	public isLogged(){
        return this.angularFireAuth.authState;
    }
    public logout(){
    	return this.angularFireAuth.auth.signOut();
    }
    public getUser(){
    	return this.angularFireAuth.auth;
    }
}