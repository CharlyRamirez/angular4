import { Component } from '@angular/core';
import { LugaresService } from '../services/lugares.services';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
	selector: 'app-lugares',
	templateUrl: './lugares.component.html',
	styleUrls: ['./lugares.component.css'],
	animations: [
		trigger('contenedorAnimable', [
			state('inicial', style({
				opacity: 0
			})),
			state('final', style({
				opacity: 1
			})),
			transition('inicial => final', animate(2000)),
			transition('final => inicial', animate(1000))
		])
	]
})
export class LugaresComponent {
	title = 'Platzi Square';
	state = 'inicial';
	lat:number = 20.557308;
    lng:number = -100.41757;
	lugares = null;

	mensajeError = null;
	mensajeErrorMostrar = false;

	animar(){
		this.state = (this.state === 'final' ? 'inicial' : 'final')
	}

	animacionInicia(e){
		console.log('Iniciado'+e)
	}
	animacionTermina(e){
		console.log('Terminado'+e)
	}
	constructor(private lugaresService: LugaresService){
		lugaresService.getLugares()
			.valueChanges().subscribe(lugares => {
			/*.subscribe(lugares => {*/
				this.lugares = lugares;
				var me = this;
				me.lugares = Object.keys(me.lugares).map(function (key) { return me.lugares[key]; });
				this.state = 'final';
			}, error => {
				//alert('Tenemos algo de dificultades, disculpe las molestias. Error: '+error.statusText);
				this.mensajeError = "El recurso web no fue localizado ("+error +")";
				this.mensajeErrorMostrar = true;
			});
	}
	cerrarMensajeError(){
		this.mensajeErrorMostrar = false;
	}
}